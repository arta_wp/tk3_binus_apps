<!DOCTYPE html>
<html>

<head>
  <title>Binus TP2</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
  <h3>User</h3>

  <a class="btn btn-success btn-sm" href="/user/create"> + Tambah User </a>

  <br />
  <br />
  <div class="table-responsive">
    <table class="table table-striped table-hover">
      <tr>
        <th>No</th>
        <th>Nama Lengkap</th>
        <th>Email</th>
        <th>Tanggal Lahir</th>
        <th>Jenis Kelamin</th>
        <th>Alamat</th>
        <th>Telp.</th>
        <th>Role</th>
        <th>Aksi</th>
      </tr>
      @foreach ($users as $key => $u)
        <tr>
          <td>{{ ++$key }}</td>
          <td>{{ $u->nama_lengkap }}</td>
          <td>{{ $u->email_corporate }}</td>
          <td>{{ $u->tanggal_lahir }}</td>
          <td>{{ $u->jenis_kelamin }}</td>
          <td>{{ $u->alamat }}</td>
          <td>{{ $u->nomor_telepon }}</td>
          <td>{{ $u->id_role }}</td>
          <td>
            <form action="{{ route('user.destroy', $u->id_user) }}" method="POST">
              <a type="button" class="btn btn-warning" href="{{ route('user.edit', $u->id_user) }}">Edit</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Hapus</button>
            </form>
          </td>
        </tr>
      @endforeach
    </table>
  </div>

</body>

</html>
