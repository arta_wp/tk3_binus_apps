<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <meta name="robots" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="users" content="{{ Cookie::get('auth_bre_data') }}" />
  <meta name="url-app" content="{{ env('APP_URL') }}" />

  <!-- PAGE TITLE HERE -->
  <title>TK3 Group 3</title>

  <!-- FAVICONS ICON -->
  <link href="{{ asset('lib/css/style.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style>
    .hide {
      display: none;
    }
  </style>
</head>

<body class="vh-100">
  <div class="authincation h-100">
    <div class="container h-100">
      <div class="row justify-content-center h-100 align-items-center">
        <div class="col-md-6">
          <div class="authincation-content">
            <div class="row no-gutters">
              <div class="col-xl-12">
                <div class="auth-form">
                  <h4 class="text-center mt-4 mb-5">Login Kelompok 7</h4>
                  <form method="POST" id="form-login">
                    {{ csrf_field() }}
                    <div class="mb-3">
                      <label class="mb-1"><strong>Email Corporate</strong></label>
                      <input required type="text" name="email_corporate" class="form-control"
                        placeholder="Your email corporate account">
                    </div>
                    <div class="mb-3">
                      <label class="mb-1"><strong>Password</strong></label>
                      <input required type="password" name="password" class="form-control"
                        placeholder="Your password account">
                    </div>
                    <div class="text-center">
                      <button id="button-login" type="submit" class="btn btn-primary btn-block"><i id="loading-login"
                          class="fa fa-spinner fa-spin hide"></i>&nbsp;
                        Sign Me In</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!--**********************************
        Scripts
    ***********************************-->
  <!-- Required vendors -->
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
  <script src="{{ asset('lib/vendor/global/global.min.js') }}"></script>
  <script src="{{ asset('lib/js/custom.min.js') }}"></script>
  <script src="{{ asset('lib/js/dlabnav-init.js') }}"></script>
  <script src="{{ asset('lib/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('lib/js/mydiary/login.js') }}"></script>
  <script>
    $('#form-login').submit(function(e) {
      $.ajax({
        type: "POST",
        url: "/user/check-login",
        data: $(this).serialize(),
        dataType: "JSON",
        success: function(response) {
          console.log({
            response
          });
          if (response.status == 'failed') {
            alert(response.message);
          } else {
            window.location.replace('/role/check-role');
          }
        }
      });
      return false;
    })
  </script>
</body>

</html>
