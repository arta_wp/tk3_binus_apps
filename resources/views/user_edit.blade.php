<!DOCTYPE html>
<html>

<head>
  <title>Binus TP2</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>

<body>

  <nav class="navbar navbar-light bg-light justify-content-between">
    <a class="navbar-brand">EDIT USER</a>
  </nav>

  <div class="container">
    <form action="{{ route('user.update', $user->id_user) }}" method="post" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="col">
        Nama Lengkap <input class="form-control" type="text" value="{{ $user->nama_lengkap }}" name="nama_lengkap"
          placeholder="Nama Lengkap" required="required"> <br />
      </div>
      <div class="col">
        Email Corporate <input class="form-control" type="text" value="{{ $user->email_corporate }}"
          name="email_corporate" placeholder="Email Corporate" required="required"> <br />
      </div>
      <div class="col">
        Password <input class="form-control" type="password" value="{{ $user->password }}" name="password"
          placeholder="Password" required="required">
        <br />
      </div>
      <div class="col">
        Tanggal Lahir <input class="form-control" type="date" value="{{ $user->tanggal_lahir }}" name="tanggal_lahir"
          placeholder="Tulis Tanggal Lahir" required="required"> <br />
      </div>
      <div class="col">
        Jenis Kelamin
        <select class="form-control" value="{{ $user->jenis_kelamin }}" name="jenis_kelamin" required>
          <option value="L">Laki-Laki</option>
          <option value="P">Perempuan</option>
        </select>
        <br>
      </div>
      <div class="col">
        Alamat <input class="form-control" type="text" value="{{ $user->alamat }}" name="alamat"
          placeholder="Alamat" required="required">
        <br />
      </div>
      <div class="col">
        Telepon <input class="form-control" type="text" value="{{ $user->nomor_telepon }}" name="nomor_telepon"
          placeholder="Nomor Telepon" required="required">
        <br />
      </div>
      <div class="col">
        Role
        <select value="{{ $user->id_role }}" name="id_role" class="form-control" required>
          @foreach ($roles as $key => $r)
            <option value="{{ $r->id_role }}"> {{ $r->nama_role }}</option>
          @endforeach
        </select>
        <br />
      </div>
      <input class="btn btn-success btn-sm" type="submit" value="Simpan Data">
      <a class="btn btn-warning btn-sm" href="/user">Batal</a>
    </form>
  </div>

</body>

</html>
