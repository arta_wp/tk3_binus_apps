/*
 Navicat Premium Data Transfer

 Source Server         : Localhsot - Mac MariaDB
 Source Server Type    : MariaDB
 Source Server Version : 100803
 Source Host           : localhost:3306
 Source Schema         : binus_tp2

 Target Server Type    : MariaDB
 Target Server Version : 100803
 File Encoding         : 65001

 Date: 24/10/2022 08:04:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id_role` int(9) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(50) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES (3, 'administrator', 'Administrator - Super User');
INSERT INTO `roles` VALUES (4, 'employee', 'Employee - Karyawan');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_user` int(9) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(150) NOT NULL,
  `email_corporate` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `alamat` text DEFAULT NULL,
  `nomor_telepon` varchar(16) DEFAULT NULL,
  `id_role` int(9) DEFAULT NULL,
  PRIMARY KEY (`id_user`,`email_corporate`),
  KEY `fk_role` (`id_role`),
  CONSTRAINT `fk_role` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id_role`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (2, 'Arta Windy', 'arta@tugas.com', '123123', '1997-01-01', 'L', '-', '0822', 3);
INSERT INTO `users` VALUES (3, 'Andri', 'andri@tugas.com', '123123', '1997-01-01', 'L', '-', '0813', 4);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
