<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function checkRole(Request $request)
    {
        $id_role = $request->session()->get('id_role');
        $role = Role::where('id_role', $id_role)->get();
        if (count($role)) {
            $nama_role = $role[0]['nama_role'];
            switch ($nama_role) {
                case 'administrator':
                    $this->roleAdministrator();
                    break;

                case 'employee':
                    $this->roleEmployee();
                    break;

                default:
                    $this->roleNotFound($id_role);
                    break;
            }
        } else {
            $this->roleNotFound($role);
        }
    }

    private function roleNotFound($id_role)
    {
        echo $id_role;
        echo 'Role tidak ditemukan, silahkan login ulang';
    }

    private function roleAdministrator()
    {
        echo 'role admininstrator';
    }

    private function roleEmployee()
    {
        echo 'role employee';
    }
}
