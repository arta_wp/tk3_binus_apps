<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    // Method berikut ini digunakan untuk menampilkan halaman login
    public function login()
    {
        return \view('login.index');
    }

    // Method berikut digunakan untuk melakukan pengecekan user
    public function checkLogin(Request $request)
    {
        $email_corporate = $request->email_corporate;
        $password = $request->password;
        $check = User::where('email_corporate', $email_corporate)->where('password', $password)->get();
        if (\count($check)) {
            $response = [
                'status' => 'success',
                'message' => 'User berhasil melakukan login',
            ];
            // Melakukan save session ketika berhasil login untuk keperluan class role
            $request->session()->put('nama_lengkap', $check[0]['nama_lengkap']);
            $request->session()->put('email_corporate', $check[0]['email_corporate']);
            $request->session()->put('id_role', $check[0]['id_role']);
        } else {
            $response = [
                'status' => 'failed',
                'message' => 'Gagal melakukan login, silahkan periksa kombinasi email dan password',
            ];
        }
        echo \json_encode($response);
    }
}
